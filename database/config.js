const moongose = require('mongoose');
require('dotenv').config();

const dbConnection = async() => {

    try {
        await moongose.connect(process.env.DB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        console.log('testUsersDB ONLINE...');
    } catch (error) {
        console.log(error);
        throw new error('Failed to connect to database...');
    }
};


module.exports = {
    dbConnection
};