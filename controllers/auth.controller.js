const { response } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user.model');
const { generateJWT } = require('../helpers/jwt');

const login = async(req, res = response) => {

    const { email, password } = req.body;

    try {

        const user = await User.findOne({ email });

        if (!user) {
            return res.status(404).json({
                ok: false,
                msg: 'Username or Password is incorrect'
            });
        }

        const validatePassword = bcrypt.compareSync(password, user.password);
        if (!validatePassword) {
            return res.status(404).json({
                ok: false,
                msg: 'Username or Password is incorrect'
            });
        }


        const token = await generateJWT(user.id);

        res.json({
            ok: true,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: `Fatal error: ${error}`
        });
    }

};


module.exports = {
    login
};