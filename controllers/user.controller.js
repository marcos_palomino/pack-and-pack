const { response } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user.model');

const getUsers = async(req, res = response) => {

    const data = req.params.data;
    const dataRegex = new RegExp(data, 'i');

    const users = await User.find({
        $or: [{ name: dataRegex }, { hobby: dataRegex }]
    });

    res.json({
        ok: true,
        users: users,
        total: users.length
    });

};
/*
const getUsersFilter = async(req, res = response) => {

    const users = await User.find({
        $and
    }, { _id: 0, name: 1, phone: 1, hobby: 1 });

    res.json({
        ok: true,
        users: users,
        total: users.length
    });

};
*/
const createUser = async(req, res = response) => {

    const { email, password } = req.body;

    try {


        const emailExists = await User.findOne({ email });

        if (emailExists) {
            return res.status(404).json({
                ok: false,
                msg: 'The email is already registered'
            });
        }

        const user = new User(req.body);

        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt);

        await user.save();

        res.json({
            ok: true,
            msg: 'Registered user',
            user
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: `Error! The user is not registered. ${error}`
        });
    }

};

const deleteUser = async(req, res = response) => {
    const uid = req.params.id;
    try {

        const user = await User.findById(uid);
        if (!user) {
            return res.status(404).json({
                ok: false,
                msg: 'There is no user with that ID...'
            });
        }

        await User.findByIdAndDelete(uid);
        res.json({
            ok: true,
            msg: 'Deleted user...'
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: `Error! The user is not deleted. ${error}`
        });
    }
};


module.exports = {
    getUsers,
    //getUsersFilter,
    createUser,
    deleteUser
};