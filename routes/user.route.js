const { Router } = require('express');
const { check } = require('express-validator');
const { validateData } = require('../middlewares/validate-data');
const { validateJWT } = require('../middlewares/validate-jwt');
const { getUsers, createUser, deleteUser } = require('../controllers/user.controller');

const router = Router();

router.get('/all/:data?', validateJWT, getUsers);
//router.get('/filter', getUsersFilter);

router.post('/', [
    validateJWT,
    check('name', 'The Name es required').not().isEmpty(),
    check('email', 'The Email es required').isEmail(),
    check('phone', 'The Phone es required').not().isEmpty(),
    check('password', 'The Password es required').not().isEmpty(),
    check('age', 'The Age es required').not().isEmpty(),
    check('gender', 'The Gender es required').not().isEmpty(),
    check('hobby', 'The Hobby es required').not().isEmpty(),
    validateData
], createUser);

router.delete('/:id', validateJWT, deleteUser);

module.exports = router;