const { Router } = require('express');
const { check } = require('express-validator');
const { validateData } = require('../middlewares/validate-data');

const { login } = require('../controllers/auth.controller');

const router = Router();

router.post('/', [
    check('password', 'The Password is required').not().isEmpty(),
    check('email', 'The Email is required').isEmail(),
    validateData
], login);

module.exports = router;