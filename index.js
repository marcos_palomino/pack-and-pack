require('dotenv').config();
const express = require('express');
const cors = require('cors');
const { dbConnection } = require('./database/config');

const app = express();

// CORS
app.use(cors());

// Parser body
app.use(express.json());

dbConnection();

//Routes
app.use('/user', require('./routes/user.route'));
app.use('/auth', require('./routes/auth.route'));

app.listen(process.env.PORT, () => {
    console.log('Starting server on port:' + process.env.PORT);
});